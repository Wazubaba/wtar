DC= dmd
DFLAGS=

BIN= wtar
DEBUGBIN= wtar-debug
TESTBIN= unittest-wtar

INCLUDES= -Isupport/libwtar/include -Isupport/libargparse/include -Isrc
i386-LIB-INCLUDE= -L-Lsupport/libwtar/i386 -L-Lsupport/libargparse/i386
x86_64-LIB-INCLUDE= -L-Lsupport/libwtar/x86_64 -L-Lsupport/libargparse/x86_64

lib-release= -L-largparse -L-lwtar
lib-debug= -L-largparse-debug -L-lwtar-debug

SRCFILES= $(shell find src -type f ! -name "*skip.*" -and ! -path "*skip.*" -name "*.d")
i386-release-objfiles= $(patsubst %.d, i386/obj-release/%.o, $(SRCFILES))
i386-debug-objfiles= $(patsubst %.d, i386/obj-debug/%.o, $(SRCFILES))
i386-test-objfiles= $(patsubst %.d, i386/obj-test/%.o, $(SRCFILES))

x86_64-release-objfiles= $(patsubst %.d, x86_64/obj-release/%.o, $(SRCFILES))
x86_64-debug-objfiles= $(patsubst %.d, x86_64/obj-debug/%.o, $(SRCFILES))
x86_64-test-objfiles= $(patsubst %.d, x86_64/obj-test/%.o, $(SRCFILES))

.PHONY: release debug test clean all package
#all: release64
all: release64

release: release32 release64
release32: support/libwtar/i386/libwtar.a support/libargparse/i386/libargparse.a i386/bin/$(BIN)
release64: support/libwtar/x86_64/libwtar.a support/libargparse/x86_64/libargparse.a x86_64/bin/$(BIN)

debug: debug32 debug64
debug32: support/libwtar/i386/libwtar-debug.a support/libargparse/i386/libargparse-debug.a i386/bin/$(DEBUGBIN)
debug64: support/libwtar/x86_64/libwtar-debug.a support/libargparse/x86_64/libargparse-debug.a x86_64/bin/$(DEBUGBIN)

test: test32 test64
test32: support/libwtar/i386/libwtar-debug.a support/libargparse/i386/libargparse-debug.a i386/bin/$(TESTBIN)
test64: support/libwtar/x86_64/libwtar-debug.a support/libargparse/x86_64/libargparse-debug.a x86_64/bin/$(TESTBIN)

docs: docs/$(BIN).html

package: tar-release zip-release

tar-release: release/$(BIN)-release.tar.gz release/$(BIN)-debug.tar.gz release/$(BIN)-source.tar.gz
zip-release: release/$(BIN)-release.zip release/$(BIN)-debug.zip release/$(BIN)-source.zip

full: release debug test docs

i386-release-libs= support/libwtar/i386/libwtar.a support/libargparse/i386/libargparse.a
i386-debug-libs= support/libwtar/i386/libwtar-debug.a support/libargparse/i386/libargparse-debug.a

x86_64-release-libs= support/libwtar/x86_64/libwtar.a support/libargparse/x86_64/libargparse.a
x86_64-debug-libs= support/libwtar/x86_64/libwtar-debug.a support/libargparse/x86_64/libargparse-debug.a

#### LIBRARY BUILD RULES ####
## libwtar
support/libwtar/i386/libwtar-debug.a:
	@cd support/libwtar; $(MAKE) debug32

support/libwtar/i386/libwtar.a:
	@cd support/libwtar; $(MAKE) release32

support/libwtar/x86_64/libwtar-debug.a:
	@cd support/libwtar; $(MAKE) debug64

support/libwtar/x86_64/libwtar.a:
	@cd support/libwtar; $(MAKE) release64

## libargparse
support/libargparse/i386/libargparse-debug.a:
	@cd support/libargparse; $(MAKE) debug32

support/libargparse/i386/libargparse.a:
	@cd support/libargparse; $(MAKE) release32

support/libargparse/x86_64/libargparse-debug.a:
	@cd support/libargparse; $(MAKE) debug64

support/libargparse/x86_64/libargparse.a:
	@cd support/libargparse; $(MAKE) release64

#### COMPILING RULES ####

#####################
# 32bit Compile rules
#####################

### Object builders ###
i386/obj-release/%.o: %.d
	$(DC) $(DFLAGS) $(INCLUDES) -m32 -c -release -O -of$@ $<

i386/obj-debug/%.o: %.d
	$(DC) $(DFLAGS) $(INCLUDES) -m32 -c -g -debug -of$@ $<

i386/obj-test/%.o: %.d
	$(DC) $(DFLAGS) $(INCLUDES) -m32 -c -g -debug -unittest -main -of$@ $<

### Bin builders ###
# i386/bin/$(BIN): $(i386-release-objfiles) $(i386-release-libs)
# 	$(DC) $(DFLAGS) $(i386-LIB-INCLUDE) -m32 -release -O -of$@ $(i386-release-objfiles)$(i386-release-libs)
#
# i386/bin/$(DEBUGBIN): $(i386-release-objfiles) $(i386-debug-libs)
# 	$(DC) $(DFLAGS) $(i386-LIB-INCLUDE) -m32 -g -debug -of$@ $<
#
# i386/bin/$(TESTBIN): $(i386-release-objfiles) $(i386-debug-libs)
# 	$(DC) $(DFLAGS) $(i386-LIB-INCLUDE) -m32 -g -debug -unittest -of$@ $<

i386/bin/$(BIN): $(i386-release-objfiles)
	@echo $(x86_64-release-objfiles)
	$(DC) $(DFLAGS) $(i386-LIB-INCLUDE) -m32 -release -O -of$@ $(i386-release-objfiles) $(lib-release)

i386/bin/$(DEBUGBIN): $(i386-debug-objfiles)
	$(DC) $(DFLAGS) $(i386-LIB-INCLUDE) -m32 -g -debug -of$@ $(i386-debug-objfiles) $(lib-debug)

i386/bin/$(TESTBIN): $(i386-test-objfiles)
	$(DC) $(DFLAGS) $(i386-LIB-INCLUDE) -m32 -g -debug -unittest -of$@ $(i386-test-objfiles) $(lib-debug)

#####################
# 64bit Compile rules
#####################

### Object builders ###
x86_64/obj-release/%.o: %.d
	$(DC) $(DFLAGS) $(INCLUDES) -m64 -c -release -O -of$@ $<

x86_64/obj-debug/%.o: %.d
	$(DC) $(DFLAGS) $(INCLUDES) -m64 -c -g -debug -of$@ $<

x86_64/obj-test/%.o: %.d
	$(DC) $(DFLAGS) $(INCLUDES) -m64 -c -g -debug -unittest -main -of$@ $<

### Bin builders ###
x86_64/bin/$(BIN): $(x86_64-release-objfiles)
	@echo $(x86_64-release-objfiles)
	$(DC) $(DFLAGS) $(x86_64-LIB-INCLUDE) -m64 -release -O -of$@ $(x86_64-release-objfiles) $(lib-release)

x86_64/bin/$(DEBUGBIN): $(x86_64-debug-objfiles)
	$(DC) $(DFLAGS) $(x86_64-LIB-INCLUDE) -m64 -g -debug -of$@ $(x86_64-debug-objfiles) $(lib-debug)

x86_64/bin/$(TESTBIN): $(x86_64-test-objfiles)
	$(DC) $(DFLAGS) $(x86_64-LIB-INCLUDE) -m64 -g -debug -unittest -of$@ $(x86_64-test-objfiles) $(lib-debug)

###############################################################################

### Documentation generation ###
docs/$(OUTNAME).html:
	@mkdir docs
	$(DC) $(DFLAGS) -o- -D -Df$@ src/$(OUTNAME).d

###############################
# PACKAGE CONSTRUCTION RULES
###############################
.$(BIN)-release-package: release docs
	@mkdir -p $@
	@cp -r i386 $@/.
	@cp -r x86_64 $@/.
	@cp -r docs $@/.
	@cp -r include $@/.
	@cp README.md $@/.

.$(BIN)-debug-package: debug docs
	@mkdir -p $@
	@cp -r i386 $@/.
	@cp -r x86_64 $@/.
	@cp -r docs $@/.
	@cp -r include $@/.
	@cp README.md $@/.

.$(BIN)-source-package: docs include/dwst/$(OUTNAME).di
	@mkdir -p $@
	@cp -r src $@/.
	@cp -r docs $@/.
	@cp -r include $@/.
	@cp README.md $@/.

release/$(BIN)-release.tar.gz: .$(BIN)-release-package
	@mkdir -p release
	@tar c $< > release/$(BIN)-release.tar
	@gzip release/$(BIN)-release.tar

release/$(BIN)-debug.tar.gz: .$(BIN)-debug-package
	@mkdir -p release
	@tar c $< > release/$(BIN)-debug.tar
	@gzip release/$(BIN)-debug.tar

release/$(BIN)-source.tar.gz: .$(BIN)-source-package
	@mkdir -p release
	@tar c $< > release/$(BIN)-source.tar
	@gzip release/$(BIN)-source.tar

release/$(BIN)-release.zip: .$(BIN)-release-package
	@mkdir -p release
	@zip $@ -r $<

release/$(BIN)-debug.zip: .$(BIN)-debug-package
	@mkdir -p release
	@zip $@ -r $<

release/$(BIN)-source.zip: .$(BIN)-source-package
	@mkdir -p release
	@zip -r $@ $<

###############################################################################

#### UTILITY RULES ####
clean:
	-@$(RM) -r i386 x86_64
	-@$(RM) -r docs
	-@$(RM) -r release .$(BIN)-release-package .$(BIN)-debug-package .$(BIN)-source-package
	@cd support/libwtar; $(MAKE) clean
	@cd support/libargparse; $(MAKE) clean

# install: release docs
# 	@mkdir -p $(DESTDIR)/usr/lib/i386-linux-gnu/.
# 	@cp i386/lib$(OUTNAME).a $(DESTDIR)/usr/lib/i386-linux-gnu/.
#
# 	@mkdir -p $(DESTDIR)/usr/lib/x86_64-linux-gnu/.
# 	@cp x86_64/lib$(OUTNAME).a $(DESTDIR)/usr/lib/x86_64-linux-gnu/.
#
# 	@mkdir -p $(DESTDIR)/usr/include/dwst
# 	@cp include/dwst$(OUTNAME).di $(DESTDIR)/usr/include/dwst/.
#
# 	@mkdir -p $(DESTDIR)/usr/share/doc/dwst/libwtar
# 	@cp docs/$(OUTNAME).html $(DESTDIR)/usr/share/doc/dwst/lib$(OUTNAME)/.
#
# uninstall:
# 	$(RM) $(DESTDIR)/usr/lib/i386-linux-gnu/lib$(OUTNAME).a
# 	$(RM) $(DESTDIR)/usr/lib/x86_64-linux-gnu/lib$(OUTNAME).a
# 	$(RM) $(DESTDIR)/usr/include/dwst/$(OUTNAME).di
# 	$(RM) -r $(DESTDIR)/usr/share/doc/dwst/lib$(OUTNAME)
