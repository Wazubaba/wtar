# W T A R

## Info
This tool acts as an interface to [libwtar](https://gitlab.com/Wazubaba/dwst-libwtar).
I tried to keep it similar to `tar`, but there are a few differences.

Notable return values are:
*	0 - success
*	1 - not enough args provided
*	2 - no operation specified
*	3 - target file for SOURCE does not exists
*	4 - extraction or validation failed
* 10 - the arg parser somehow broke

## Usage
See `wtar --help` for more information.

## Compiling
`Wtar` uses submodules, so you will need to execute `git submodule init` and then
`git submodule update` to pull in the two libraries it uses. After that, you can
either issue `make` or `dub`.

Make is the recommended method as dub is not the most reliable thing on the block.

The makefile has numerous rules:
*	debug[32|64]
*	test[32|64]
*	release[32|64]
*	package

## Installation
`Wtar` is a flat binary. Throw it somewhere in your path and
when you want to remove it, just delete it :)
