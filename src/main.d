import std.stdio: writeln, writefln;
import std.path: asAbsolutePath, baseName, buildPath, extension;
import std.array: array;
import std.zlib: compress, uncompress;
import std.file: read, write, exists, getcwd, mkdir;
import std.conv: to;
import std.string: lastIndexOf;

import dwst.argparse;
import dwst.wtar;

/// Help displayed on invocation of --help
string LONGHELP =
"Usage: wtar MODE TARGETS

You should specify a mode flag of either c, e, or t, otherwise nothing will be
done.

	-c, --create         create a new archive from SOURCE at [DESTINATION], or
                            as SOURCE.wtar
	-e, --extract        extract data from SOURCE
	-g, --gzip           on create, gzip the output and output .gz to the end
	                         of the create file. On extract, run the data
								    through gzip before attempting to extract
	-h, --help           show this help and exists
	-o, --output         specifiy output file name, default sources the name of current directory
	-t, --test       test data integrity. Ensure you are using gzip mode if
                            necessary, or this will fail
	-v, --verbose        show information while running
	
Examples:
	# Create wtar-source-release.wtar with the directories src and support
	wtar -c -o wtar-source-release src support
	# Extract those directories into testdir
	wtar -e -o testdir wtar-source-release.wtar";


int main(string[] args)
{
	Opts opts;
	bool ARGHELP, ARGCREATE, ARGEXTRACT, ARGVALIDATE, ARGGZIP, ARGVERBOSE = false;
	string[] outputname;

	opts.register("-c", "--create", &ARGCREATE); // TODO: Should this just be c?
	opts.register("-e", "--extract", &ARGEXTRACT); // TODO: Should this just be e?
	opts.register("-g", "--gzip", &ARGGZIP);
	opts.register("-h", "--help", &ARGHELP);
	opts.register("-o", "--output", null, 1, &outputname);
	opts.register("-t", "--test", &ARGVALIDATE);
	opts.register("-v", "--verbose", &ARGVERBOSE);

	try
	{
		args.parse(opts);
	}
	catch (NotEnoughSubArgs e)
	{ // Can never be too careful ;-)
		writeln("ERROR: you somehow broke the argument parser!");
		writeln("Please file a bug at https://gitlab.com/Wazubaba/neowst-d-argparse/issues with this error and a short description of what you were doing:)");
		writeln(e.msg);
		return 10;
	}

	// output name defaults to the name of the current directory
	if (outputname.length == 0)
	{
		if (ARGCREATE)
			outputname ~= getcwd().baseName ~ ".wtar";
		else
			outputname ~= ".";
	} 

	if (ARGHELP)
	{
		writeln(LONGHELP);
		return 0;
	}

	if (args.length < 3)
	{
		writeln("wtar: missing operand");
		writeln("Try 'wtar --help' for more information.");
		return 1;
	}

	string[] sources;

	foreach (i, arg; args[1..$]) // Skip arg 0
	{
		if (arg[0] == '-') continue; // Just throw these files out. Yes I know files can be prefixed with this, however if you need this submit a patch :P
		if (i > 0 && args[i] == "-o") continue; // Because -o needs an arg, this will skip that arg
		if (ARGVERBOSE) writeln("Added '" ~ arg ~ "'"); 
		sources ~= buildPath(getcwd(), arg);
	}

	string dest = outputname[0];
	
	if (ARGCREATE)
	{
		if (ARGGZIP)
		{
			if (dest.length < 8 || dest[$-8..$] != ".wtar.gz") dest ~= ".wtar.gz";
		}
		else
		{ // This for some reason never gets called unless it's in a {} block... gg dlang -_-
			if (dest.length < 5 || dest[$-5..$] != ".wtar") dest ~= ".wtar";
		}
	}

	if (ARGVERBOSE) writeln("Target file: '" ~ dest ~ "'");

	if (ARGCREATE)
	{
		auto archive = new ArchivalData();

		foreach (source; sources)
		{
			if (!source.exists)
			{
				if (ARGVERBOSE) writeln("'", source, "' does not exist!");
				return 3;
			}

			archive.ScanTargetDirectory(source);
		}
			
		string archivedata = archive.Serialize();

		if (ARGGZIP)
		{
			auto t = compress(archivedata);
			write(dest, t);
		}
		else
		{
			auto t = archivedata;
			write(dest, t);
		}	

	} else
	if (ARGEXTRACT)
	{
		foreach (source; sources)
		{
			try
			{
				auto data = read(source);
				string stream;
				if (ARGGZIP)
					stream = to!string(uncompress(data));
				else
					stream = to!string(data);

				auto archive = new ArchivalData();
				archive.Inject(stream);
				if (!dest.exists)
					dest.mkdir();
				archive.Inflate(dest);
			}
			catch (Exception e)
			{
				if (ARGVERBOSE) writeln("Error decompressing archive '", source, "'");
				if (ARGVERBOSE) writeln("Error message: \n\n", e.msg);
				return 4;
			}
		}
	} else
	if (ARGVALIDATE)
	{
		foreach (source; sources)
		{
			try
			{
				auto data = read(source);
				string stream;
				if (ARGGZIP)
					stream = to!string(uncompress(data));
				else
					stream = to!string(data);

				auto archive = new ArchivalData();
				archive.Inject(stream);
			}
			catch (Exception e)
			{
				if (ARGVERBOSE) writeln("Test Failed. '", source, "' appears to be corrupted.");
				if (ARGVERBOSE) writeln("Error message: \n\n", e.msg);
				return 1;
			}
		}
	}
	else
	{
		if (ARGVERBOSE) writeln("No operation specified, nothing to do!");
		return 2;

	}
	return 0;
}
